var soap = require('soap')
  , xml2js = require('xml2js')
  , builder = new xml2js.Builder()
  , parser = new xml2js.Parser({ explicitArray : false });

/**
 * Faz requisição ao Webservice do OAS
 * @param OasUrl - url wsdl
 * @param OasAcount - Account ID do usuário
 * @param OasUser - User ID
 * @param OasPass - Senha
 * @param xml - string do xml a ser enviado para o WebService
 * @param callback - função a ser executada com o retorno do WebService
 * @return undefined
 * */
function requestOas( OasUrl, OasAccount, OasUser, OasPass, xml, callback, ntry){
  ntry = ntry || 0;
  soap.createClient(OasUrl, function(err, client) {
    
    if ( err ) {
      callback(err);
      return;
    } 

    client.OasXmlRequest({
      String_1 : OasAccount,
      String_2 : OasUser,
      String_3 : OasPass,
      String_4 : xml
    }, function( err, data ) {

      if ( err ) {
        err = { errorStatus : err.code, errorMessage : 'SOAP error' };
      }

      //var parser = new XmlParser({ explicitArray : false });

      if ( data ) {
        console.log('Tentativa %d', ntry + 1);
        if ( ! data.result ) { // xml incompleto
          /* tenta por, no máximo, 5 vezes */
          if ( ntry++ <= 5 ) {
            requestOas( OasUrl, OasAccount, OasUser, OasPass, xml, callback, ntry);
          } else {
            callback( { errorStatus : -2, errorMessage : 'API indisponível' } );
          }
        } else { 
          parser.parseString(data.result, function(err, data) { 
            callback(err, data.AdXML.Response);
          });
        }
      } else { 
        callback(err, data);
      }

    });
  });
}

function OasXmlTemplate(action, type, requests) {
  var xml = {}
    , elements
    , request
    , tag
    , attrs;

  if ( requests instanceof Array ) {
    xml.AdXML = { Request : [] }; 

    for( var i = 0, l = requests.length; i < l; i++ ){
      elements = requests[i]; 

      request = {
        $ : {
          type : OasReqAttrValue( action, type )
        }
      }; 

      tag = OasTagName( action, type );
      attrs = OasAttributes( action, type);

      request[tag] = elements;
      request[tag].$ = attrs;

      xml.AdXML.Request.push( request );
    }
  
  } else {

    xml.AdXML = {
      Request : {
        $ : {
          type : OasReqAttrValue( action, type )
        }
      }
    };

    tag = OasTagName( action, type );
    attrs = OasAttributes( action, type );

    xml.AdXML.Request[ tag ] = requests; 
    xml.AdXML.Request[ tag ].$ = attrs

  }

  return xml;
}

function OasReqAttrValue( action, type ) {
  switch( action.toLowerCase() ) {
    case "report" :
      return action;
      break;
    default :
      return type;
      break;
  } 
}

function OasTagName( action, type ) {
  if ( action.match(/report/i) ){
    return action;
  }

  switch( type.toLowerCase() ) {
    case "campaign" : 
    case "creative" :
    case "creativetarget" :
      return type;
      break;
    default :
      return "Database";
      break;
  }
}

function OasAttributes( action, type ){
  return action.match(/report|frequency|inventory/i) ? { type : type } : { action : action };
}

function OasApi(host, account, user, pass) { 
  
  this.url = host + 'oas/wsdl/oasApi.wsdl';
  this.account = account;
  this.user = user;
  this.pass = pass;

}

OasApi.prototype.list = function(type, requests, callback) { 
  requests = requests || { SearchCriteria : {} };

  var OasXml = builder.buildObject( OasXmlTemplate( 'list', type, requests ) );

  console.log(OasXml);

  requestOas( this.url, this.account, this.user, this.pass, OasXml, callback );
};

OasApi.prototype.update = function(obj, callback) { 
  // TODO
};

OasApi.prototype.add = function(type, request, callback) { 
  // TODO
};

OasApi.prototype.read = function(type, requests, callback) { 
  requests = requests || {};

  var OasXml = builder.buildObject( OasXmlTemplate( 'read', type, requests ) );

  console.log(OasXml);

  requestOas( this.url, this.account, this.user, this.pass, OasXml, callback );
}; 

OasApi.prototype.report = function(type, requests, callback) {
  requests = requests || {};

  var OasXml = builder.buildObject( OasXmlTemplate( 'Report', type, requests ) );

  console.log(OasXml);

  requestOas( this.url, this.account, this.user, this.pass, OasXml, callback );
  
}; 

module.exports = OasApi;
